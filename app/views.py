from django.shortcuts import render, redirect, HttpResponse
import pymysql
from utils import sqlhelp
import time


def test(request):
    return render(request, 'test.html')


def login1(request):
    '''
    管理员登录
    :param request:
    :return:
    '''
    # if request.method == 'GET':
    #     return render(request, 'login.html')
    # else:
    u = request.POST.get('name')
    p = request.POST.get('password')
    v = sqlhelp.get_list2('select * from super_user where sup_user=%s', [u, ])

    if v == None:
        return HttpResponse('nok')
    else:
        if u == v.get('sup_user') and p == v.get('sup_password'):
            obj = HttpResponse('ok')
            obj.set_signed_cookie('name', u, '123')
            return obj
        else:
            return HttpResponse('nok')


def login(request):
    '''
    普通用户登录
    :param request:
    :return:
    '''
    if request.method == 'GET':
        return render(request, 'login.html')
    else:
        u = request.POST.get('name')
        p = request.POST.get('password')
        v = sqlhelp.get_list2('select * from user where username=%s', [u, ])
        if v == None:
            return HttpResponse('nok')
        else:
            if u == v.get('username') and p == v.get('password'):
                obj = HttpResponse('ok')
                obj.set_signed_cookie('name', u, salt='123')
                return obj
            else:
                return HttpResponse('nok')


def index(request):
    return render(request, 'index.html')


def superuser(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
        print(tk)
    except:
        return redirect('/login/')
    aa = {'name': tk}
    superuser_list = sqlhelp.get_list('select * from super_user', [])
    return render(request, 'superuser.html', {'superuser_list': superuser_list, 'aa': aa})


def user(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
        print(tk)
    except:
        return redirect('/login/')
    aa = {'name': tk}
    user_list = sqlhelp.get_list('select * from user', [])
    return render(request, 'user.html', {'user_list': user_list, 'aa': aa})


def adduser(request):
    u = request.POST.get('name')
    p = request.POST.get('password')
    i = request.POST.get('id')
    a = sqlhelp.get_list2('select * from user where username=%s', [u])
    b = sqlhelp.get_list2('select * from super_user where sup_user=%s', [u])
    if a != None or b != None:
        return HttpResponse('该用户名已存在,请重新输入')
    else:
        try:
            if len(u) > 0 and len(p) > 0 and len(i) > 0:
                sqlhelp.modify('insert into user(userid,username,password) values(%s,%s,%s)', [i, u, p])
                return HttpResponse('ok')
            else:
                return HttpResponse('全部信息不能为空')
        except:
            return HttpResponse('该员工id已存在,请重新输入')


def deluser(request):
    '''
    删除用户
    :param request:
    :return:
    '''
    nid = request.GET.get('nid')
    sqlhelp.modify('delete from user where userid=%s', [nid, ])
    return redirect('/user/')


def edituser(request):
    '''
    编辑用户
    :param request:
    :return:
    '''

    nid = request.POST.get('id')
    nusername = request.POST.get('name')
    npassword = request.POST.get('password')
    a = sqlhelp.get_list2('select * from user where username=%s', [nusername])
    b = sqlhelp.get_list2('select * from super_user where sup_user=%s', [nusername])
    if a != None or b != None:
        return HttpResponse('该用户名已存在,请重新输入')
    else:
        if len(nid) > 0 and len(nusername) > 0 and len(npassword) > 0:
            sqlhelp.modify('update user set username=%s,password=%s where userid=%s', [nusername, npassword, nid, ])
            return HttpResponse('ok')
        else:
            return HttpResponse('全部信息不能为空')


def bill(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
        print(tk)
    except:
        return redirect('/login/')
    aa = {'name': tk}
    '''
    查看单据
    :param request:
    :return:
    '''

    bill_list = sqlhelp.get_list('select * from bill_commodity', [])
    # bill_list["name"]=tk

    for i in bill_list:
        i['bill_commodity_produtiondate'] = (str(i['bill_commodity_produtiondate']))
        i['bill_commodity_date'] = (str(i['bill_commodity_date']))
    return render(request, 'bill.html', {'bill_list': bill_list, 'aa': aa})


def bill2(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
        print(tk)
    except:
        return redirect('/login/')
    aa = {'name': tk}
    '''
    查看出货单据
    :param request:
    :return:
    '''
    bill2_list = sqlhelp.get_list('select * from bill2_commodity', [])
    for i in bill2_list:
        i['bill2_commodity_produtiondate'] = (str(i['bill2_commodity_produtiondate']))
        i['bill2_commodity_date'] = (str(i['bill2_commodity_date']))
    return render(request, 'bill2.html', {'bill2_list': bill2_list, 'aa': aa})


def addbilla(request):
    title = request.POST.get('title')
    print(title)
    return HttpResponse('ok')


def addbill(request):
    '''
    添加单据
    :param request:
    :return:
    '''

    a = request.POST.get('id')
    b = request.POST.get('commodity_id')
    c = request.POST.get('commodity_name')
    d = request.POST.get('commodity_unit')
    e = request.POST.get('commodity_num')
    f = request.POST.get('commodity_produtiondate')
    g = request.POST.get('commodity_safedate')
    h = request.POST.get('commodity_date')
    i = request.POST.get('commodity_money')
    try:
        if len(a) > 0 and len(b) > 0 and len(c) > 0 and len(d) > 0 and len(e) > 0 and len(f) > 0 and len(g) > 0 and len(
                h) > 0 and len(i) > 0:
            sqlhelp.modify(
                'insert into wh_commodity(wh_commodity_id,wh_commodity_name,wh_commodity_unit,wh_commodity_num,wh_commodity_safedate,wh_commodity_produtiondate) values(%s,%s,%s,%s,%s,%s)',
                [b, c, d, e, g, f])
            sqlhelp.modify(
                'insert into bill_commodity(bill_id,bill_commodity_id,bill_commodity_name,bill_commodity_unit,bill_commodity_num,bill_commodity_money,bill_commodity_date,bill_commodity_produtiondate,bill_commodity_safedate) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                [a, b, c, d, e, i, h, f, g])

            return HttpResponse('ok')
        else:
            return HttpResponse('添加失败,全部内容都不能为空')
    except:
        return HttpResponse('该商品id已存在,请重新添加')


def addbill2(request):
    a = request.POST.get('id')
    b = request.POST.get('commodity_id')
    c = request.POST.get('commodity_name')
    d = request.POST.get('commodity_unit')
    e = request.POST.get('commodity_num')
    f = request.POST.get('commodity_produtiondate')
    g = request.POST.get('commodity_safedate')
    i = request.POST.get('commodity_money')
    h = time.strftime("20%y-%m-%d")
    j = int(request.POST.get('numcha'))
    if len(a) > 0 and len(b) > 0 and len(c) > 0 and len(d) > 0 and len(e) > 0 and len(f) > 0 and len(g) > 0 and len(
            h) > 0 and len(i) > 0:
        sqlhelp.modify(
            'insert into bill2_commodity(bill2_id,bill2_commodity_id,bill2_commodity_name,bill2_commodity_unit,bill2_commodity_num,bill2_commodity_money,bill2_commodity_date,bill2_commodity_produtiondate,bill2_commodity_safedate) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)',
            [a, b, c, d, e, i, h, f, g])
        if j > 0:
            sqlhelp.modify(
                'UPDATE  wh_commodity SET wh_commodity_num=%s WHERE wh_commodity_id=%s',
                [j, b])
        else:

            sqlhelp.modify('delete from wh_commodity where wh_commodity_id=%s', [b, ])
        return HttpResponse('ok')
    else:
        return HttpResponse('添加失败,全部内容都不能为空')


def editbill(request):
    import datetime
    '''
    编辑单据
    :param request:
    :return:
    '''
    id1 = request.POST.get('id1')
    id2 = request.POST.get('id2')
    commodity = request.POST.get('commodity')
    unit = request.POST.get('unit')
    num = request.POST.get('num')
    produtiondate = request.POST.get('produtiondate')
    safedate = request.POST.get('safedate')
    money = request.POST.get('money')
    date = request.POST.get('date')
    if len(id1) > 0 and len(id2) > 0 and len(commodity) > 0 and len(unit) > 0 and len(num) > 0 and len(
            produtiondate) > 0 and len(safedate) > 0 and len(money) > 0 and len(date) > 0:
        sqlhelp.modify('delete from bill_commodity where bill_commodity_id=%s', [id2, ])
        # sqlhelp.modify('delete from wh_commodity where wh_commodity_id=%s', [id2, ])
        sqlhelp.modify(
            'insert into bill_commodity(bill_id,bill_commodity_id,bill_commodity_name,bill_commodity_unit,bill_commodity_num,bill_commodity_money,bill_commodity_date,bill_commodity_produtiondate,bill_commodity_safedate) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)',
            [id1, id2, commodity, unit, num, money, date, produtiondate, safedate, ])
        # sqlhelp.modify(
        #     'insert into wh_commodity(wh_commodity_id,wh_commodity_name,wh_commodity_unit,wh_commodity_num) values(%s,%s,%s,%s)',
        #     [id2, commodity, unit, num, ])
        return HttpResponse('ok')
    else:
        return HttpResponse('编辑内容不能为空')
def editbill2(request):
    import datetime
    '''
    编辑单据
    :param request:
    :return:
    '''
    id3 = request.POST.get('id3')
    id1 = request.POST.get('id1')
    id2 = request.POST.get('id2')
    commodity = request.POST.get('commodity')
    unit = request.POST.get('unit')
    num = request.POST.get('num')
    produtiondate = request.POST.get('produtiondate')
    safedate = request.POST.get('safedate')
    money = request.POST.get('money')
    date = request.POST.get('date')
    print(id3)
    if len(id1) > 0 and len(id2) > 0 and len(commodity) > 0 and len(unit) > 0 and len(num) > 0 and len(
            produtiondate) > 0 and len(safedate) > 0 and len(money) > 0 and len(date) > 0:
        sqlhelp.modify('delete from bill2_commodity where id=%s', [id3, ])
        # sqlhelp.modify('delete from wh_commodity where wh_commodity_id=%s', [id2, ])
        sqlhelp.modify(
            'insert into bill2_commodity(id,bill2_id,bill2_commodity_id,bill2_commodity_name,bill2_commodity_unit,bill2_commodity_num,bill2_commodity_money,bill2_commodity_date,bill2_commodity_produtiondate,bill2_commodity_safedate) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
            [id3,id1, id2, commodity, unit, num, money, date, produtiondate, safedate, ])
        # sqlhelp.modify(
        #     'insert into wh_commodity(wh_commodity_id,wh_commodity_name,wh_commodity_unit,wh_commodity_num) values(%s,%s,%s,%s)',
        #     [id2, commodity, unit, num, ])
        return HttpResponse('ok')
    else:
        return HttpResponse('编辑内容不能为空')


def delbill(request):
    '''
    删除进货单单
    :param request:
    :return:
    '''
    nid = request.GET.get('nid')
    sqlhelp.modify('delete from bill_commodity where bill_commodity_id=%s', [nid, ])
    # sqlhelp.modify('delete from wh_commodity where wh_commodity_id=%s', [nid, ])
    return redirect('/bill/')


def delbill2(request):
    '''
    删除进货单单
    :param request:
    :return:
    '''
    nid = request.GET.get('nid')
    sqlhelp.modify('delete from bill2_commodity where bill2_commodity_id=%s', [nid, ])
    # sqlhelp.modify('delete from wh_commodity where wh_commodity_id=%s', [nid, ])
    return redirect('/bill2/')


def wh(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
        print(tk)
    except:
        return redirect('/login/')
    aa = {'name': tk}
    wh_list = sqlhelp.get_list('select * from wh_commodity', [])
    for i in wh_list:
        i['wh_commodity_produtiondate'] = str(i['wh_commodity_produtiondate'])
    return render(request, 'wh.html', {'wh_list': wh_list, 'aa': aa})


def addwh(request):
    id = request.POST.get('wh_commodity_id')
    name = request.POST.get('wh_commodity_name')
    unit = request.POST.get('wh_commodity_unit')
    num = request.POST.get('wh_commodity_num')
    produtiondate = request.POST.get('wh_commodity_produtiondate')
    safedate = request.POST.get('wh_commodity_safedate')
    print("id")
    print(id)
    try:
        if len(id) > 0 and len(name) > 0 and len(unit) > 0 and len(num) > 0 and len(produtiondate) > 0 and len(
                safedate) > 0:
            sqlhelp.modify(
                'insert into wh_commodity(wh_commodity_id,wh_commodity_name,wh_commodity_unit,wh_commodity_num,wh_commodity_produtiondate,wh_commodity_safedate) value (%s,%s,%s,%s,%s,%s)',
                [id, name, unit, num, produtiondate, safedate, ])
            return HttpResponse('ok')
        else:
            return HttpResponse('全部数据不能为空!')
    except:
        return HttpResponse('该商品id已存在,请重新添加')


def delwh(request):
    nid = request.GET.get('nid')
    sqlhelp.modify('delete from wh_commodity where wh_commodity_id=%s', [nid, ])
    # sqlhelp.modify('delete from wh_commodity where wh_commodity_id=%s', [nid, ])
    return redirect('/wh/')


def editwh(request):
    if request.method == 'GET':
        id = request.GET.get('id')
        editwh_list = sqlhelp.get_list2(
            'select wh_commodity_id,wh_commodity_name,wh_commodity_num,wh_commodity_unit from wh_commodity where wh_commodity_id=%s',
            [id, ])
        return render(request, 'editwh.html', {'editwh_list': editwh_list})
    else:
        num1 = int(request.POST.get('num1'))
        num2 = int(request.POST.get('num2'))
        unit = request.POST.get('unit')
        name = request.POST.get('name')
        id = request.POST.get('id')
        null = 'null'
        print(num1)
        print(num2)
        print(id)

        if 0 <= num2 <= num1:
            num = num1 - num2
            print(num)
            sqlhelp.modify('update wh_commodity set wh_commodity_num=%s where wh_commodity_id=%s', [num, id, ])
            sqlhelp.modify(
                'insert into sm_commodity(sm_commodity_id,sm_commodity_name,sm_commodity_unit,sm_commodity_num) values (%s,%s,%s,%s)',
                [id, name, unit, num])
            return redirect('/wh/')
        else:
            return render(request, 'editwh.html')


def sm(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
        print(tk)
    except:
        return redirect('/login/')
    aa = {'name': tk}
    sm_list = sqlhelp.get_list('select * from sm_commodity', [])
    for i in sm_list:
        i['sm_commodity_produtiondate'] = str(i['sm_commodity_produtiondate'])
    return render(request, 'sm.html', {'sm_list': sm_list, 'aa': aa})


def addsm(request):
    b = request.POST.get('sm_commodity_id')
    c = request.POST.get('sm_commodity_name')
    d = request.POST.get('sm_commodity_unit')
    e = request.POST.get('sm_commodity_num')
    f = request.POST.get('sm_commodity_produtiondate')
    g = request.POST.get('sm_commodity_safedate')
    i = request.POST.get('sm_commodity_money')
    j = int(request.POST.get('numcha'))
    if len(b) > 0 and len(c) > 0 and len(d) > 0 and len(e) > 0 and len(f) > 0 and len(g) > 0 and len(i) > 0:
        gg = sqlhelp.get_list2('select * from sm_commodity where sm_commodity_id=%s', [b])
        if gg == None:
            sqlhelp.modify(
                'insert into sm_commodity(sm_commodity_id,sm_commodity_name,sm_commodity_unit,sm_commodity_num,sm_commodity_prcie,sm_commodity_produtiondate,sm_commodity_safedate) values(%s,%s,%s,%s,%s,%s,%s)',
                [b, c, d, e, i, f, g])
        else:
            sqlhelp.modify(
                'update sm_commodity set sm_commodity_num=sm_commodity_num+%s,sm_commodity_prcie=%s where sm_commodity_id=%s',
                [e, i, b])
        if j > 0:
            sqlhelp.modify(
                'UPDATE  wh_commodity SET wh_commodity_num=%s WHERE wh_commodity_id=%s',
                [j, b])
        else:
            sqlhelp.modify('delete from wh_commodity where wh_commodity_id=%s', [b, ])
        return HttpResponse('ok')

    else:
        return HttpResponse('添加失败,全部内容都不能为空')


def zhangmu(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
        print(tk)
    except:
        return redirect('/login/')
    aa = {'name': tk}
    zhangmu_list = sqlhelp.get_list('select * from zhangmu', [])
    for i in zhangmu_list:
        i['zhangmu_date'] = str(i['zhangmu_date'])
    yingyee = (sqlhelp.get_list('select sum(zhangmu_money) a from zhangmu where zhangmu_type=%s', ['收入', ]))[0]
    chengben = (sqlhelp.get_list('select sum(zhangmu_money) a from zhangmu where zhangmu_type=%s', ['支出', ]))[0]
    if yingyee.get('a') == None:
        yingyee['a'] = 0
    if chengben.get('a') == None:
        chengben['a'] = 0

    lirun = {'a': float(yingyee.get('a')) - float(chengben.get('a'))}

    return render(request, 'zhangmu.html',
                  {'zhangmu_list': zhangmu_list, 'aa': aa, 'yingyee': yingyee, 'chengben': chengben, 'lirun': lirun})


def addzhangmu(request):
    name = request.POST.get('commodity_name')
    num = request.POST.get('commodity_num')
    id = request.POST.get('commodity_id')
    date = time.strftime("20%y-%m-%d")
    type = '收入'
    summoney = request.POST.get('commodity_money')
    j = int(request.POST.get('numcha'))

    if len(name) > 0 and len(num) > 0 and len(date) > 0 and len(summoney) > 0:
        sqlhelp.modify(
            'insert into zhangmu(zhangmu_name,zhangmu_num,zhangmu_date,zhangmu_type,zhangmu_money) values (%s,%s,%s,%s,%s)',
            [name, num, date, type, summoney])
        if j > 0:
            sqlhelp.modify(
                'UPDATE  sm_commodity SET sm_commodity_num=%s WHERE sm_commodity_id=%s',
                [j, id])
        else:
            sqlhelp.modify('delete from sm_commodity where sm_commodity_id=%s', [id, ])
        return HttpResponse('ok')

    else:
        return HttpResponse('全部数据不能为空')


def editsm(request):
    id = request.POST.get('commodity_id')
    money = request.POST.get('commodity_money')
    if len(id) > 0 and len(money) > 0:
        sqlhelp.modify('update sm_commodity set sm_commodity_prcie=%s where sm_commodity_id=%s', [money, id])
        return HttpResponse('ok')
    else:
        return HttpResponse('全部信息不能为空')


def delsuser(request):
    nid = request.GET.get('nid')
    sqlhelp.modify('delete from super_user where sup_id=%s', [nid, ])
    return redirect('/superuser/')


def addsuser(request):
    u = request.POST.get('name')
    p = request.POST.get('password')
    i = request.POST.get('id')
    a = sqlhelp.get_list2('select * from user where username=%s', [u])
    b = sqlhelp.get_list2('select * from super_user where sup_user=%s', [u])
    if a != None or b != None:
        return HttpResponse('该用户名已存在,请重新输入')
    else:
        try:
            if len(u) > 0 and len(p) > 0 and len(i) > 0:
                sqlhelp.modify('insert into super_user(sup_id,sup_user,sup_password) values(%s,%s,%s)', [i, u, p])
                return HttpResponse('ok')
            else:
                return HttpResponse('全部信息不能为空')
        except:
            return HttpResponse('该员工id已存在,请重新输入')


def editsuser(request):
    nid = request.POST.get('id')
    nusername = request.POST.get('name')
    npassword = request.POST.get('password')
    a = sqlhelp.get_list2('select * from user where username=%s', [nusername])
    b = sqlhelp.get_list2('select * from super_user where sup_user=%s', [nusername])
    if a != None or b != None:
        return HttpResponse('该用户名已存在,请重新输入')
    else:
        if len(nid) > 0 and len(nusername) > 0 and len(npassword) > 0:
            sqlhelp.modify('update super_user set sup_user=%s,sup_password=%s where sup_id=%s',
                           [nusername, npassword, nid, ])
            return HttpResponse('ok')
        else:
            return HttpResponse('全部信息不能为空')


def quanxian(request):
    name = request.POST.get('name')
    a = sqlhelp.get_list2('select * from super_user where sup_user=%s', [name])
    if a == None:
        return HttpResponse('nok')
    else:
        return HttpResponse('ok')


def zhangmuchaxun(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
    except:
        return redirect('/login/')
    aa = {'name': tk}
    date = request.POST.get('year') + '-' + '_' + request.POST.get('month') + '%'
    zhangmu_list = sqlhelp.get_list('select * from zhangmu where zhangmu_date like %s', [date])
    zhangmulist = sqlhelp.get_list('select * from zhangmu', [])
    for i in zhangmu_list:
        i['zhangmu_date'] = str(i['zhangmu_date'])
    for i in zhangmulist:
        i['zhangmu_date'] = str(i['zhangmu_date'])
    print(zhangmu_list)
    c = {'msg': '查询信息不存在'}
    if len(zhangmu_list) == 0:
        yingyee = (
            sqlhelp.get_list('select sum(zhangmu_money) a from zhangmu where zhangmu_type=%s ',
                             ['收入', ]))[0]
        chengben = (
            sqlhelp.get_list('select sum(zhangmu_money) a from zhangmu where zhangmu_type=%s ',
                             ['支出', ]))[0]
        lirun = {'a': float(yingyee.get('a')) - float(chengben.get('a'))}
        return render(request, 'zhangmu.html',
                      {'c': c, 'zhangmu_list': zhangmulist, 'aa': aa, 'yingyee': yingyee, 'chengben': chengben,
                       'lirun': lirun}, )
    else:
        yingyee = (
            sqlhelp.get_list('select sum(zhangmu_money) a from zhangmu where zhangmu_type=%s and zhangmu_date like %s',
                             ['收入', date]))[0]
        chengben = (
            sqlhelp.get_list('select sum(zhangmu_money) a from zhangmu where zhangmu_type=%s and zhangmu_date like %s',
                             ['支出', date]))[0]
        if yingyee.get('a') == None:
            yingyee['a'] = 0
        if chengben.get('a') == None:
            chengben['a'] = 0
        lirun = {'a': float(yingyee.get('a')) - float(chengben.get('a'))}
        return render(request, 'zhangmu.html',
                      {'zhangmu_list': zhangmu_list, 'aa': aa, 'yingyee': yingyee, 'chengben': chengben,
                       'lirun': lirun})


def delzhangmu(request):
    nid = request.GET.get('nid')
    sqlhelp.modify('delete from zhangmu where zhangmu_id=%s', [nid, ])
    return redirect('/zhangmu/')


def editzhangmu(request):
    name = request.POST.get('name')
    type = request.POST.get('type')
    num = request.POST.get('num')
    date = request.POST.get('date')
    money = request.POST.get('money')
    id = request.POST.get('id')
    if len(name) > 0 and len(type) > 0 and len(num) > 0 and len(date) > 0 and len(money) > 0:
        sqlhelp.modify(
            'update zhangmu set zhangmu_name=%s,zhangmu_type=%s,zhangmu_num=%s,zhangmu_date=%s,zhangmu_money=%s where zhangmu_id=%s',
            [name, type, num, date, money, id])
        return HttpResponse('ok')
    else:
        return HttpResponse('全部内容不能为空')


def billchaxun(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
    except:
        return redirect('/login/')
    aa = {'name': tk}
    id = request.POST.get('id') + '%'
    id2 = request.POST.get('id2') + '%'
    name = request.POST.get('name')
    print(name)
    print('id2:' + id2)
    if len(name) == 0:
        name = '%'
    else:
        name = '%' + name + '%'
    produtiondate = request.POST.get('produtiondate') + '%'
    date = request.POST.get('date') + '%'
    bill_list = sqlhelp.get_list(
        'select * from bill_commodity where bill_id like %s and bill_commodity_id like %s and bill_commodity_name like %s and bill_commodity_produtiondate like %s and bill_commodity_date like %s',
        [id, id2, name, produtiondate, date])
    for i in bill_list:
        i['bill_commodity_produtiondate'] = (str(i['bill_commodity_produtiondate']))
        i['bill_commodity_date'] = (str(i['bill_commodity_date']))
    return render(request, 'bill.html', {'bill_list': bill_list, 'aa': aa})


def bill2chaxun(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
    except:
        return redirect('/login/')
    aa = {'name': tk}
    id = request.POST.get('id') + '%'
    id2 = request.POST.get('id2') + '%'
    name = request.POST.get('name')
    if len(name) == 0:
        name = '%'
    else:
        name = '%' + name + '%'
    produtiondate = request.POST.get('produtiondate') + '%'
    date = request.POST.get('date') + '%'
    bill2_list = sqlhelp.get_list(
        'select * from bill2_commodity where bill2_id like %s and bill2_commodity_id like %s and bill2_commodity_name like %s and bill2_commodity_produtiondate like %s and bill2_commodity_date like %s',
        [id, id2, name, produtiondate, date])
    for i in bill2_list:
        i['bill2_commodity_produtiondate'] = (str(i['bill2_commodity_produtiondate']))
        i['bill2_commodity_date'] = (str(i['bill2_commodity_date']))
    return render(request, 'bill2.html', {'bill2_list': bill2_list, 'aa': aa})


def whchaxun(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
    except:
        return redirect('/login/')
    aa = {'name': tk}
    id2 = request.POST.get('id2') + '%'
    name = request.POST.get('name')
    if len(name) == 0:
        name = '%'
    else:
        name = '%' + name + '%'
    produtiondate = request.POST.get('produtiondate') + '%'
    wh_list = sqlhelp.get_list(
        'select * from wh_commodity where wh_commodity_id like %s and wh_commodity_name like %s and wh_commodity_produtiondate like %s ',
        [id2, name, produtiondate])
    for i in wh_list:
        i['wh_commodity_produtiondate'] = (str(i['wh_commodity_produtiondate']))
    return render(request, 'wh.html', {'wh_list': wh_list, 'aa': aa})


def smchaxun(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
    except:
        return redirect('/login/')
    aa = {'name': tk}
    id2 = request.POST.get('id2') + '%'
    name = request.POST.get('name')
    if len(name) == 0:
        name = '%'
    else:
        name = '%' + name + '%'
    produtiondate = request.POST.get('produtiondate') + '%'
    print(id2)
    sm_list = sqlhelp.get_list(
        'select * from sm_commodity where sm_commodity_id like %s and sm_commodity_name like %s and sm_commodity_produtiondate like %s ',
        [id2, name, produtiondate])
    for i in sm_list:
        i['sm_commodity_produtiondate'] = (str(i['sm_commodity_produtiondate']))
    return render(request, 'sm.html', {'sm_list': sm_list, 'aa': aa})


def userchaxun(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
    except:
        return redirect('/login/')
    aa = {'name': tk}
    id2 = request.POST.get('id2') + '%'
    name = request.POST.get('name')
    if len(name) == 0:
        name = '%'
    else:
        name = '%' + name + '%'
    print(id2)
    user_list = sqlhelp.get_list('select * from user where userid like %s and username like %s ', [id2, name])
    return render(request, 'user.html', {'user_list': user_list, 'aa': aa})


def suserchaxun(request):
    try:
        tk = request.get_signed_cookie('name', salt='123')
    except:
        return redirect('/login/')
    aa = {'name': tk}
    id2 = request.POST.get('id2') + '%'
    name = request.POST.get('name')
    if len(name) == 0:
        name = '%'
    else:
        name = '%' + name + '%'
    print(id2)
    superuser_list = sqlhelp.get_list('select * from super_user where sup_id like %s and sup_user like %s ',
                                      [id2, name])
    return render(request, 'superuser.html', {'superuser_list': superuser_list, 'aa': aa})
